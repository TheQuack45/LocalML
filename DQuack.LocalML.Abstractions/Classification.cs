﻿using System;

namespace DQuack.LocalML.Abstractions
{
    public struct Classification
    {
        #region Static members definition
        private const string TO_STRING_FORMAT = "Tag={0}, Probability={1}";
        #endregion Static members definition

        public string Tag { get; }
        public double Probability { get; }

        public Classification(string tag, double probability)
        {
            this.Tag = tag;
            this.Probability = probability;
        }

        public override string ToString()
        {
            return String.Format(TO_STRING_FORMAT, this.Tag, this.Probability);
        }
    }
}
