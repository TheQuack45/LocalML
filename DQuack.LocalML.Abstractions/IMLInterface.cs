﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DQuack.LocalML.Abstractions
{
    public interface IMLInterface
    {
        string ModelName { get; set; }

        void Initialize(string modelName,
                        string inputLabel,
                        string outputLabel,
                        (int Width, int Height) inputSize,
                        int outputSize,
                        string dataPath = null);
        Task<IReadOnlyList<Classification>> ClassifyImage(Stream image);
    }
}
