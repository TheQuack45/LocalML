﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace DQuack.LocalML.Android
{
    public static class BitmapExtensions
    {
        public static float[] GetBitmapPixels(this Bitmap bitmap, int width, int height,
                                              float imageMeanR, float imageMeanG, float imageMeanB)
        {
            // Array is a flattened version of the image. columns * rows * 3 (one for each color channel).
            var floatValues = new float[width * height * 3];

            using (Bitmap scaledBitmap = Bitmap.CreateScaledBitmap(bitmap, width, height, false))
            {
                using (Bitmap resizedBitmap = scaledBitmap.Copy(Bitmap.Config.Argb8888, false))
                {
                    var intValues = new int[width * height];
                    resizedBitmap.GetPixels(intValues, 0, resizedBitmap.Width, 0, 0, resizedBitmap.Width, resizedBitmap.Height);

                    for (int i = 0; i < intValues.Length; ++i)
                    {
                        // TODO: They did this in the original library. Is it necessary? It's some sort of normalization.
                        //    floatValues[i * 3 + 0] = ((val & 0xFF) - imageMeanB) / ImageStd;
                        //    floatValues[i * 3 + 1] = (((val >> 8) & 0xFF) - imageMeanG) / ImageStd;
                        //    floatValues[i * 3 + 2] = (((val >> 16) & 0xFF) - imageMeanR) / ImageStd;

                        floatValues[i * 3 + 0] = intValues[i] & 0xFF;
                        floatValues[i * 3 + 1] = (intValues[i] >> 8) & 0xFF;
                        floatValues[i * 3 + 2] = (intValues[i] >> 16) & 0xFF;
                    }

                    resizedBitmap.Recycle();
                }

                scaledBitmap.Recycle();
            }

            return floatValues;
        }
    }
}