﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DQuack.LocalML.Abstractions;
using DQuack.LocalML.Android;
using Org.Tensorflow.Contrib.Android;

namespace DQuack.LocalML
{
    public class MLInterface : IMLInterface
    {
        #region Members definition
        #region Static members definition
        // TODO: Get rid of these?
        private const float IMAGE_MEAN_R = 123.0f;
        private const float IMAGE_MEAN_G = 117.0f;
        private const float IMAGE_MEAN_B = 104.0f;

        private const string MODEL_GRAPH_FORMAT = "{0}.pb";
        private const string MODEL_LABELS_FORMAT = "{0}.Labels.txt";
        #endregion Static members definition

        #region Properties definition
        public string ModelName { get; set; }
        public string InputLabel { get; private set; }
        public string OutputLabel { get; private set; }

        public (int Width, int Height) InputSize { get; private set; }
        #endregion Properties definition

        #region Fields definition
        private List<string> _labels;
        private TensorFlowInferenceInterface _inferenceInterface;
        #endregion Fields definition
        #endregion Members definition

        #region Methods definition
        public void Initialize(string modelName,
                               string inputLabel, string outputLabel,
                               (int Width, int Height) inputSize, int outputSize,
                               string dataPath = null)
        {
            if (string.IsNullOrEmpty(modelName))
                { throw new ArgumentException("The given model name is invalid.", nameof(modelName)); }

            this.ModelName = modelName;
            this.InputLabel = inputLabel;
            this.OutputLabel = outputLabel;
            this.InputSize = inputSize;

            if (dataPath == null)
            {
                dataPath = Application.Context.FilesDir.Path;
            }

            string modelsDir = System.IO.Path.Combine(dataPath, "Models");

            this.LoadModel(modelName, modelsDir);

            if (this._labels.Count != outputSize)
            {
                this._inferenceInterface = null;
                this.InputLabel = null;
                this.OutputLabel = null;
                this._labels.Clear();
                this._labels = null;

                throw new ArgumentException("The given output size did not match the size found from the labels file.");
            }
        }

        private void LoadModel(string modelName, string dataPath)
        {
            string modelPath = System.IO.Path.Combine(dataPath,
                                                      String.Format(MODEL_GRAPH_FORMAT, modelName));
            string labelsPath = System.IO.Path.Combine(dataPath,
                                                       String.Format(MODEL_LABELS_FORMAT, modelName));

            using (var reader = new StreamReader(labelsPath))
            {
                string content = reader.ReadToEnd();
                this._labels = content.Split('\n').Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s)).ToList();
            }

            using (var model = new FileStream(modelPath, FileMode.Open, FileAccess.Read))
            {
                this._inferenceInterface = new TensorFlowInferenceInterface(model);
            }
        }

        public async Task<IReadOnlyList<Classification>> ClassifyImage(Stream image)
        {
            if (this._labels == null || !this._labels.Any() || this._inferenceInterface == null)
                { throw new InvalidOperationException("The implementation must be initialized before use by calling TfInterfaceImplementation.Initialize."); }

            using (Bitmap bitmap = await BitmapFactory.DecodeStreamAsync(image))
            {
                // TODO: Why does this have to be done with the Task.Run? TensorFlow interface is a little strange.
                IReadOnlyList<Classification> classifications = await Task.Run(() => this.IdentifyImage(bitmap).AsReadOnly());
                bitmap.Recycle();
                return classifications;
            }
        }

        private List<Classification> IdentifyImage(Bitmap bitmap)
        {
            var outputNames = new string[] { this.OutputLabel, };
            var floatValues = bitmap.GetBitmapPixels(this.InputSize.Width, this.InputSize.Height,
                                                     IMAGE_MEAN_R, IMAGE_MEAN_G, IMAGE_MEAN_B);
            var outputs = new float[this._labels.Count];

            this._inferenceInterface.Feed(this.InputLabel,
                                          floatValues,
                                          1,
                                          this.InputSize.Width,
                                          this.InputSize.Height,
                                          3);
            this._inferenceInterface.Run(outputNames);
            this._inferenceInterface.Fetch(this.OutputLabel, outputs);

            var results = new List<Classification>();
            for (int i = 0; i < outputs.Length; i++)
            { results.Add(new Classification(this._labels[i], outputs[i])); }

            return results;
        }
        #endregion Methods definition
    }
}