﻿using CoreGraphics;
using CoreML;
using CoreVideo;
using DQuack.LocalML.Abstractions;
using DQuack.LocalML.iOS;
using Foundation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIKit;
using Vision;

namespace DQuack.LocalML
{
    public class MLInterface : IMLInterface
    {
        #region Members definition
        #region Static members definition
        private const string MODEL_FORMAT = "{0}.mlmodel";
        private const string COMPILED_MODEL_FORMAT = "{0}.mlmodelc";
        #endregion Static members definition

        #region Properties definition
        public string ModelName { get; set; }
        public CGSize TargetImageSize { get; private set; }
        #endregion Properties definition

        #region Members definition
        private VNCoreMLModel _model;
        #endregion Members definition
        #endregion Members definition

        public void Initialize(string modelName,
                               string inputLabel, string outputLabel,
                               (int Width, int Height) inputSize, int outputSize,
                               string dataPath = null)
        {
            if (string.IsNullOrEmpty(modelName))
                { throw new ArgumentException("The given model name is invalid.", nameof(modelName)); }

            this.ModelName = modelName;
            this.TargetImageSize = new CGSize(inputSize.Width, inputSize.Height);

            if (dataPath == null)
            {
                string documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                dataPath = Path.Combine(documents, "..", "Library", "Caches");
            }

            // TODO: Should we be always going to the Models directory? That should probably be up to the user.
            string modelsDir = dataPath.EndsWith("Models/") ? dataPath : Path.Combine(dataPath, "Models");

            this._model = this.LoadModel(modelName, modelsDir);
        }

        private VNCoreMLModel LoadModel(string modelName, string dataPath)
        {
            string compiledModelPathStr = Path.Combine(dataPath, String.Format(COMPILED_MODEL_FORMAT, modelName));
            NSUrl compiledModelPath = new NSUrl(compiledModelPathStr, false);

            if (compiledModelPath == null)
            { throw new ArgumentException("The given model was not found.", nameof(modelName)); }

            MLModel mlModel;
            NSError e = null;
            try
            {
                mlModel = MLModel.Create(compiledModelPath, out e);
            }
            catch (NSErrorException)
            {
                // TODO: Will the above ever throw NSErrorException? Is this catch necessary?

                // This exception will be thrown if the compiled model file cannot be found.
                // Therefore we will compile it manually and provide the path to that.
                string uncompiledPath = Path.Combine(dataPath, String.Format(MODEL_FORMAT, modelName));
                compiledModelPath = CompileModel(uncompiledPath);
            }

            if (e != null)
            {
                // This error most likely means that the compiled model file cannot be found.
                // Therefore we will compile it manually and provide the path to that.
                string uncompiledPath = Path.Combine(dataPath, String.Format(MODEL_FORMAT, modelName));
                compiledModelPath = CompileModel(uncompiledPath);
            }

            mlModel = MLModel.Create(compiledModelPath, out e);

            if (e != null)
            { throw new NSErrorException(e); }

            VNCoreMLModel model = VNCoreMLModel.FromMLModel(mlModel, out e);

            if (e != null)
            { throw new NSErrorException(e); }

            return model;
        }

        private NSUrl CompileModel(string modelPath)
        {
            NSUrl uncompiled = new NSUrl(modelPath);
            NSUrl compiledModel = MLModel.CompileModel(uncompiled, out NSError e);

            if (e != null)
            { throw new NSErrorException(e); }

            return compiledModel;
        }

        private async Task<IReadOnlyList<Classification>> Classify(UIImage source)
        {
            var tcs = new TaskCompletionSource<IEnumerable<Classification>>();

            var request = new VNCoreMLRequest(this._model, (response, e) =>
            {
                if (e != null)
                {
                    tcs.SetException(new NSErrorException(e));
                }
                else
                {
                    VNClassificationObservation[] results = response.GetResults<VNClassificationObservation>();
                    tcs.SetResult(results.Select(r => new Classification(r.Identifier, r.Confidence)).ToList());
                }
            });

            CVPixelBuffer buffer = source.ToCVPixelBuffer(this.TargetImageSize);
            var requestHandler = new VNImageRequestHandler(buffer, new NSDictionary());

            requestHandler.Perform(new[] { request, }, out NSError error);

            IEnumerable<Classification> classifications = await tcs.Task;

            if (error != null)
            { throw new NSErrorException(error); }

            return classifications.OrderByDescending(p => p.Probability)
                                  .ToList()
                                  .AsReadOnly();
        }

        public async Task<IReadOnlyList<Classification>> ClassifyImage(Stream stream)
        {
            if (this._model == null)
            { throw new InvalidOperationException("The implementation must be initialized before use by calling TfInterfaceImplementation.Initialize."); }

            UIImage image = await stream.ToUIImage();
            return await Classify(image);
        }
    }
}
